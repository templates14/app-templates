# Deploy a docker registry in K8s

We will deploy a private docker registry to a K3s cluster. (recommended [k3s-boot.sh](https://juanmatiasdelacamara.wordpress.com/2020/02/26/missing-k3s-reset/))

## Certificates

Create a certificate and the secret to store it:

```
openssl req \
  -newkey rsa:4096 -nodes -sha256 -keyout tls.key \         
  -x509 -days 365 -out tls.crt

kubectl create secret -n registry generic tls-secret  \
        --from-file=./tls.key \
        --from-file=./tls.crt \

```

## The Deploy

Deploy the registry:

```
helm3 install registry --set secrets.htpasswd="kungfu:0NyqzuOb9xs1JVMA2q" --set tlsSecretName="tls-secret" --namespace registry stable/docker-registry
```

_If you plan to access the registry from outside the cluster add "--set service.type=LoadBalancer"._

This is a very simple example, setting user and password through htpasswd. (must create these values using `htpasswd` command)


## Access registry from host

If you are using K3s (recommended [k3s-boot.sh](https://juanmatiasdelacamara.wordpress.com/2020/02/26/missing-k3s-reset/)), you can add a line to `/etc/hosts`, binding registry-docker-registry (the name of your service), domain name to the service IP, and access registry there.

To get the IP:

```
kubectl get svc -n registry registry-docker-registry
```

_Add this IP and registry-docker-registry as hostname in your /etc/hosts._

Then you will need to add the cert to docker local 

```
sudo mkdir -p /etc/docker/certs.d/registry-docker-registry:5000/
sudo cp ./tls.crt /etc/docker/certs.d/registry-docker-registry:5000/ 
```

Now you can `docker login` as usual.

## Access registry from cluster

You need to set the credentials on your cluster. Create the file `/etc/rancher/k3s/registries.yaml` and add this content:

```
---
mirrors:
  registry-docker-registry:5000:
    endpoint:
      - "https://registry-docker-registry:5000"
configs:
  registry-docker-registry:5000:
    auth:
      username: kungfu
      password: <yourpassword>
    tls:
      ca_file: /etc/rancher/k3s/cert.d/tls.crt
      cert_file: /etc/rancher/k3s/cert.d/tls.crt
      key_file: /etc/rancher/k3s/cert.d/tls.key
```

Create the `cert.d` dir and copy there your files:

```
sudo mkdir -p /etc/rancher/k3s/cert.d
cp ./tls* /etc/rancher/k3s/cert.d
```

Restart K3s:

```
k3s-boot.sh stop
k3s-boot.sh start -k /tmp/k3s-kc
export KUBECONFIG=/tmp/k3s-kc
```

Now, you can check k3s has added registry config looking into this file `/var/lib/rancher/k3s/agent/etc/containerd/config.toml`.

_More info [here](https://rancher.com/docs/k3s/latest/en/installation/airgap/)._

Ok, you are ready to push your images to the registry and deploy containers from them. Enjoy and don't forget to revisit the Marx Brothers videos.

