# linkerd quick start

Following this site: https://linkerd.io/2/getting-started/

```
# install client on localhost
curl -sL https://run.linkerd.io/install | sh
# and add it yo your PATH as you prefer

# Validate cluster
linkerd check --pre

# Install linkerd
linkerd install | kubectl apply -f -

# Wait until is up and running
linkerd check
```

Now, to a service to be added to the mesh you need to inject the proxy into it.

The way to indicate Linkerd a service must be injected is using an this annotation:

```
linkerd.io/inject: enabled
```

This annotation can be at any level: namespace, deployment, or pod.

(already running deployments must be restarted to be injected)

Watch your dashboard:

```
linkerd dashboard & 
```
